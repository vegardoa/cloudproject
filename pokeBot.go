package pokebot

import (
	"fmt"
	"github.com/bwmarrin/discordgo" // External library from git
	"strings"
	//"net/http"
)

// Global variables
const token string = "NjQ0ODk0NDQ3ODE2OTMzMzc2.Xc6q_g.N-QKUCQ3m9y7__QxU_VbYtYTEnk"// Token holds the bot unique token ID
const botPrefix string ="!" 		// Prefix for bot commands

//var BotID string

// BotInit initializes the bot
func BotInit() {
	dg, err := discordgo.New("Bot "+ token) // Creates discord session

	if err != nil {
		println("Error 503, service is unavailable)", err)
	}

	_,	err = dg.User("@me") // Gets user information

	if err != nil {
		println("Error 500, unexpected error)", err)           //checks if data was fetched, prints this error if this fails
	}

	//BotID = u.ID

	dg.AddHandler(messageHandler)
	//botJoinServer(dg)

	err = dg.Open() // Opens websocket connection to discord

	if err != nil {
		println("Error 503, service is unavailable)", err)      //error message if bot cant connect to server
	}

	fmt.Println("Bot should be online") // Prints text to console to indicate if bot is working
}

                                                        // Reads the text in discord and prints out an appropriate response
func messageHandler(s *discordgo.Session, m *discordgo.MessageCreate) {
	if strings.HasPrefix(m.Content, botPrefix) {        // Checks if the text contains the correct prefix
		if m.Author.Bot {                               // Checks if written message is from the bot itself
			return
		}

		input := strings.Split(m.Content, " ")

		switch input[0] {                                //a switch is used to check what command as been chosen, the input[x] inside the switch chases makes suer that the correct amount of inputs 
		case botPrefix + "testOutput":                   //has been written for the chosen command
			_, _ = s.ChannelMessageSend(m.ChannelID, "Hello, I'm a bot.")
		case botPrefix + "register":
			_, _ = s.ChannelMessageSend(m.ChannelID, registerPlayer(input[1], m.Author.ID))
		case botPrefix + "delete":
			_, _ = s.ChannelMessageSend(m.ChannelID, deletePlayer(input[1]))
		case botPrefix + "deleteTeam":  //semi functional
			_, _ = s.ChannelMessageSend(m.ChannelID, deleteTeam(input[1], input[2]))
		case botPrefix + "help":
			printAllBotCommands(s, m)
		case botPrefix + "testPic": // TEMP UNFINISHED
			_, _ = s.ChannelMessageSend(m.ChannelID, "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/1.png")
		case botPrefix + "createTeam": 
			_, _ = s.ChannelMessageSend(m.ChannelID, registerTeam(input[1], input[2]))
		case botPrefix + "addPokemon": 
			_, _ = s.ChannelMessageSend(m.ChannelID, addPokemon(input[1], input[2], input[3]))
		case botPrefix + "deletePokemon": // TEMP UNFINISHED
			_, _ = s.ChannelMessageSend(m.ChannelID, deletePokemon(input[1], input[2], input[3]))
		case botPrefix + "addItem": 
			_, _ = s.ChannelMessageSend(m.ChannelID, addItem(input[1], input[2], input[3], input[4]))
		case botPrefix + "addMove": 
			_,_ = s.ChannelMessageSend(m.ChannelID, addMove(input[1], input[2], input[3], input[4]))
		case botPrefix + "displayAllDataPlayer": // Displays all data registered to a specific player
			_, _ = s.ChannelMessageSend(m.ChannelID, getAllPlayerData(input[1])) ////////-----!!!!!!!!
		case botPrefix + "displayPlayer": // TEMP UNFINISHED Displays specific data registered to player
			_, _ = s.ChannelMessageSend(m.ChannelID, getPlayerData2(input[1])) ////////----
		case botPrefix + "displayTeamPokemon": // TEMP UNFINISHED
		_,_ = s.ChannelMessageSend(m.ChannelID, GetPlayerTeamPokemon(input[1], input[2], input[3]))
		default:
			return
		}


		//fmt. Println(m.Content) //Prints all text that's written in discord to console
	}
}

func printAllBotCommands (s *discordgo.Session, m *discordgo.MessageCreate) { // Prints out all available bot commands to discord
	_, _ = s.ChannelMessageSend(m.ChannelID, "!testOutput, Prints out the test message" +
		"\n!help, Prints out all the available bot commands" +
		"\n!register <playerName>, Registers a new player" +
		"\n!delete <playerName>, Deletes the player with playerName"+
		"\n!deleteTeam <playerName> <teamName>, Deletes a team of player -broken- should delete a team inside player"+
		"\n!createTeam <playerName> <teamName>, Creates a team for player with playerName, only one team per player, existing team gets overwritten"+
		"\n!addPokemon <playerName> <teamName> <pokemonName>, Adds a pokemon inside the players team, only one pokemon per team, existing pokemon gets overwritten"+
		"\n!deletePokemon <playerName> <teamName> <pokemonName>, -broken- should delete a pokemon inside a players team"+
		"\n!addItem <playerName> <teamName> <pokemonName> <itemName>, Adds an item to a pokemon inside the players team, only one item per pokemon, existing item gets overwritten"+
		"\n!addMove <playerName> <teamName> <pokemonName> <moveName>, Adds a move to a pokemon inside the players team, only one move per pokemon, existing move gets overwritten"+
		"\n!displayAllDataPlayer <playerName>, displays all data that is registered to a specific player")
}

/*func botJoinServer(s *discordgo.Session) {
	var serverInv string = "https://discord.gg/Gg8ArY"
	_,_ = s.InviteAccept(serverInv)

}*/
