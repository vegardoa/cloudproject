module pokebot

go 1.13.1

require (
	cloud.google.com/go/firestore v1.1.0
	firebase.google.com/go v3.10.0+incompatible
	github.com/bwmarrin/discordgo v0.20.1
	golang.org/x/crypto v0.0.0-20191002192127-34f69633bfdc // indirect
	golang.org/x/net v0.0.0-20191007182048-72f939374954 // indirect
	google.golang.org/api v0.14.0
)
