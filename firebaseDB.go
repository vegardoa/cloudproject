package pokebot

import (
	"encoding/json"
	//"net/http"
	"context"
	"time"
	"fmt"
	//"log"
	"cloud.google.com/go/firestore"
	//"google.golang.org/api/iterator"
)

// DBpath - path to key file enabling us to access the firebase database
const DBpath string = "../discordpokebot-2ba08-firebase-adminsdk-ke6k2-f08fa888a6.json"
// Col - constant that points at layer collection
const Col string = "players"
// Ctx - Context for initialising firebase
var Ctx context.Context
// FireDB - Database client pointer
var FireDB *firestore.Client
// Uptime for database
var Uptime time.Time

// Players struct for info about players
type Players struct{
	Creator    		string 			`json:"creator"`
	PlayerName 		string 			`json:"playerName"`
	TeamList 		[]PkmnTeam 		`json:"teamList"`
}

// PkmnTeam struct for info about teams
type PkmnTeam struct{
	TeamName    	string			`json:"teamName"`
	Pokemon			[]Pokemon	 		`json:"pokeList"`
}

// Pokemon struct for info about pokemon
type Pokemon struct{
	PokeName   string					`json:"pokeName"`
	Type  		 string					`json:"type"`
	Item 			 item						`json:"item"`
	Moves		 []moves					`json:"moves"`
}

type item struct{
	Category	string					`json:"category"`
	Cost			string					`json:"cost"`
	Effect		string					`json:"effect"`
	item			string					`json:"item"`
}

type moves struct{
	Accuracy	string					`json:"accuracy"`
	Effect		string					`json:"effect"`
	MoveName	string					`json:"moveName"`
	Power			string					`json:"power"`
	Pp				string					`json:"pp"`
	Type			string					`json:"type"`
}

// Checks if player exists in firestore Database
func checkPlayer(pName string) bool{
	_, err := FireDB.Collection("players").Doc(pName).Get(Ctx)
	if err != nil {
	    println("An error has occurred: %s", err) //not found
		return false
	}

	return true

}





// Seemingly fetching all wanted data, however currently only one team, one pokemon, and one move can exist.
// because of how player.go functions work (now formating data differently for firebase prevents making new array slots)
func getPkmnCount(pName string) {
	fetchedPlayer, err := FireDB.Collection("players").Doc(pName).Get(Ctx)
	if err != nil {
		println("nooo")
	}
	pData := fetchedPlayer.Data()
	jsonPData, err := json.Marshal(pData)
	if err != nil {
		println("Could not marshal map")
	}

  println(string(jsonPData))

	var p Players
	json.Unmarshal(jsonPData, &p)
	fmt.Println(p)
}



func getSpecificTeam(pName string, tName string) (PkmnTeam, bool) {
	fetchedPlayer, err := FireDB.Collection("players").Doc(pName).Get(Ctx)
	if err != nil {
		println("nooo")
	}
	pData := fetchedPlayer.Data()
	jsonPData, err := json.Marshal(pData)
	if err != nil {
		println("Could not marshal map")
	}

	var p Players
	json.Unmarshal(jsonPData, &p)
	for i:=0; i < len(p.TeamList); i++ {
		if p.TeamList[i].TeamName == tName {
			return p.TeamList[i], false
		}
	}
	return p.TeamList[0], true
}
