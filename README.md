# cloudProject

To test the bot you can join this server: https://discord.gg/THT4yfr
Or
You can add the bot manually to your server, note that the bot requires admin access. 
Use this link: https://discordapp.com/api/oauth2/authorize?client_id=644894447816933376&permissions=8&scope=bot

To check the database go to (heroku link) and use the endpoints. 
There ar two endpoints that can be used 
/pokebot/v1/players/all/
/pokebot/v1/players/specific/{playerName}

/pokebot/v1/players/all/                  // Displays all the players and all data that is registered to said players.
/pokebot/v1/players/specific/{playerName} // Enter the player name to show all data registered  to one specific player.

You should clone the repo and create your own discord bot, put its token in the token variable, and create your own firebase and replace the firebase  json with your own, when testing this application.



---Welcome to our project!---
This project is an app that uses a discord bot to preform a number of commands to register players with pokemon teams.
We are using a pokemon api to fetch some information and we are storing all of the data in firebase database.


A total of about 98 hours were spent between us four in making this discord bot application

The bot commands:

!register playerName:
This simply creates a collection named "Players" and adds a document with playerName as its name, creatorID field with teh ID of the user that created the player and an empty teamList field
This function works as it should

!delete playerName:
This deletes the document inside "Players" collection and every field inside the document aswell.
This function works as it should

!createTeam playerName teamName:
This creates a team inside the teamList field inside the player document and also initialises an empty pokemonlist to put pokemon inside it.
While this works fine, the method we have used to fetch the document: "	_, err := FireDB.Collection("players").Doc(pName).Set(Ctx, map[string]interface{}{   "
causes a big where the user can create a new player document by typing in a playerName that is not in the firebase database.
The reason is because of Set(Ctx, ....), It essentially updates the document by replacing it or creating a new one if it does not exist.
We have tried using a check for the playerName but even when the if condition is false (i.e when the player name does not exist) the program runs the createTeam command perfectly while also issuing the
404 error message.
The set.(ctx...) is used in other commands too, all the add functions, so this bug is present there aswell.

!deleteTeam playerName teamName:
This function should delete the chosen team field and its values, however there is a bug where any attempts at setting the "path" to anything else but "teamList" ends up giving an error
An error has occurred: %s (0xcc84c0,0xc000540590)  which indicates that it cannot fetch the data from this path. We have no clue why this happens and neither did the teaching assistant.
this is half functioning because we can still delete teams, but all of them instead of one.

!addPokemon playerName teamName pokemonName:
This adds a pokemon inside a team's pokelist, this function uses a pokemon api where it compares the pokemonName form input to a list of pokemon names in the pokemon api.
Here the code is stopped if the pokemonName does not exist inside the pokemon api. The function will also give the pokemon details automatically about its type.
Whats strange about this function and teh following ones is that should you write a pokemonName that does not exist, the cmd will cease to work with the app and this error is issued:
"invalid character 'N' looking for beginning of value"     While its okay since it stops users from putting in pokemons that dont exist, it does not print out the error 404. But its functional

!addItem playerName teamName pokemonName itemName:
This fills the item field in the pokemon field with the name of an item and automatically fetches information about the item such as effect, cost and item category. This function as the same weir error message
as addpokemon command.

!AddMove playerName teamName pokemonName:
This fills the move field in the pokemon field with the name of a move and automatically fetches information about the move such as effect, pp, power value, accuracy value and what type teh move has. 
This function as the same weir error message as addpokemon command.

!displayAllDataPlayer playerName prints out all data registrered to a specific player
---extras---

We have several small problems.

Error handling is wonky, most times it prints the wrong error message, this needs to be fixed
we have implemented a url function that displays data inside the firebase on a website or postman, but have problems with displaying a specific field similar to the two delete functions
further commenting is needed


more comming soon! -finished writing 22.11.2019 13:03-



this is written after 22nd november:
New additions to functions such as a !help that displays all the commands for the discord bot
The bug in the add function has been fixed with a check on if item or move actually exists, alltho for team the replacing bug still presists because there was no good way of fetching field information directly.
The same bug, the set.(ctx.....) one, was the reason for a big where only one object could be in an array in firebase. Because every add function would only change on the index[0] object, and thus every time
add functions were used, it would replace the aleready existing object instead of becoming a new object at index[1].
When it comes to delete functions, firebase golang has no good way of fetching the value of fields since it has no field or child function, and the query functions were not much help either, they would return nothing
and promt the 404 error or would return something the path errors would (An error has occurred: %s (0xcc84c0,0xc000540590)) and hand out the error 500
and as stated before, the path still would not return the desired value, only the same error: An error has occurred: %s (0xcc84c0,0xc000540590)
To fetch the information from the database to display in website has finally been dealt with and the functions that display database data on website are mostly functional with minor bugs
it was fixed by changing the data structure in database so that, but due to this we were unable to put in more than one field in teamlist, pokemon list and movelist
even after this change the delete functions remain broken allthough something was fetched, it always promted the error 500.
Error handling works better now, the "'N' looking for beginning of value" has mostly vanished from the cmd console.

