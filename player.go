package pokebot

import (
	//"encoding/json"
	//"net/http"
	"cloud.google.com/go/firestore"
	"encoding/json"
	"pokebot/pokeapi"
	"strings"
	//"fmt"
)

// Player struct for player info
type Player struct {
	playerName						string `json:"playerName"`
}

func registerPlayer(pName string, discordID string) string{
	_, err := FireDB.Collection("players").Doc(pName).Set(Ctx, map[string]interface{}{ //sets context inside the document with the playerName from collection "players" in firebase database
		"playerName": pName,                                                           //sets playername, the creator of the player and initializes an empty teamlist
		"creator": discordID,
		"teamList": "",
	})
	if err != nil {                                                                    //these error checks are common here, but basically if err != nil then data in teh database could not be altered
		println("Error 503, service is unavailable)", err)
		return "service is unavaliable."
	}
	return pName
}

func deletePlayer(pName string) string{
   if checkPlayer(pName) == true { //checks if the player exists in firebase
		_, err := FireDB.Collection("players").Doc(pName).Delete(Ctx)       //find and delete context inside document with the same name as pName, inside teh collection "players"
			if err != nil {
			println("Error 500, unexpected error)", err)           //checks if data was fetched, prints this error if this fails
			return "something is wrong." 
		}
		return "The player has been deleted."
	}else {println("Error 404, not found")                         //if the playeCheck fails, it means the player does not exist and cant be found
	return "Player was not found and could not be deleted"}

}

func registerTeam(pName string, tName string) string{

  if(checkPlayer(pName) == true){                                                              //check if the player exists in list, this now works 
	_, err := FireDB.Collection("players").Doc(pName).Set(Ctx, map[string]interface{}{         //sets context inside the document with the playerName from collection "players" in firebase database
		"teamList": []interface{}{                                               //this part creates fields within the document in firebase, it initializes a teamlist with a team whose name is taken from input tName
			map[string]interface{}{                                               //and initializes an empty pokemon list
				"teamName": tName,
				"pokeList": []interface{}{
				},                                                                //this code currently registers a team regardless if the player is aleready registered
				},                                                                //it will just create a new document if the pName is not in the firebase and put the team in there
	  },
	}, firestore.MergeAll)
	if err != nil {                                                               //if the information couldnt be fetched
	   println("Error 500, unexpected error)", err)           //checks if data was fetched, prints this error if this fails
			return "something is wrong."                                          //the bot will write this message
	}
	}else {println("Error 404, not found")
	return "Player was not found"                                                       //error code that is printed in discord bot if the player does not exist
	}




return "OK"
}

func deleteTeam(pName string, tName string) string{ //half-functioning, setting path to "teamList" deletes teamlist but nothing else
  if checkPlayer(pName) == true {                   //check if the player exists in list
  
    _, err1 := getSpecificTeam(pName, tName)        //check if the team is in list, this always returns false for now
    if err1{
	_, err := FireDB.Collection("players").Doc("tester").Update(Ctx, []firestore.Update{      //the code finds the document inside a collection and should update it
        {                                                                                     //and should be able to delete the field with the same name as "tName"
		    Path: tName,                                                                      //this for some reason will not point to fields inside fields, even with field/field pathing
            Value: firestore.Delete,                                                          //This deletes the values inside the field that "path" points to
        },})
       if err != nil {
         println("Error 500, unexpected error)", err)           //checks if data was fetched, prints this error if this fails
			return "something is wrong."                        
        }
		} else {return "Team was not found"                            //error code printed if the team was not found
		 println("Error 404, team not found)", err1) }
     }else {println("Error 404, not found")                   //error code that is printed in discord bot if the player does not exist
    return "Player was not found" }
	return "Command finished."
}

func addPokemon(pName string, tName string, pokemonName string) string{      //adds pokemons to the pokemonlist field in the team field, takes 3 arguments

	pokeObject, ex := pokeapi.GetPokemon(pokemonName)                            //this fetches information from the pokemon API to see if the name types in belongs to a pokemon in the list

	var types string
	if len(pokeObject.PokemonTypes) < 2 {
		types = pokeObject.PokemonTypes[0].Type.PokeTypeName				// Makes one combined string of all the pokemon's types
	} else {
		types = pokeObject.PokemonTypes[0].Type.PokeTypeName + ", " + pokeObject.PokemonTypes[1].Type.PokeTypeName
	}


	if ex {                                                   //if teh pokemon exists, run the code
		_, err := FireDB.Collection("players").Doc(pName).Set(Ctx, map[string]interface{}{  //fetches the player document inside "players" collection in firebase and sets the context
			"playerName": pName,
			"teamList": []interface{}{                                                      //initialise teamlist, the team name and pokemonlist and then adds the pokemon
				map[string]interface{}{
				"teamName": tName,
				"pokeList": []interface{}{
							map[string]interface{}{
							"pokeName": pokemonName,
							"type": types,                                       //the type of the pokemon is automatically fetched when the pokemon is added to the list
							"item": "",                                                      //item and moves are left open for the functions below
							"moves": "",
						},
					},
				},
			},
		}, )
		if err != nil {                                                             //if fetching information from database fails
			println("Error 500, unexpected error)", err)           //checks if data was fetched, prints this error if this fails
			return "something is wrong." 
		}
		//getPkmnCount(tName)                                                         //this gets the amount of pokemon in the registered team, this does not work right now
		return "OK"

	}else{
	    println("Error 404, not found")                                       //if the pokemon was not found in the pokemon api, this error is printed
		return "Pokemon doesn't exist, or cannot be found in the API"}
	return "Command finished."
}

func deletePokemon(pName string, tName string, pokemonName string) string{                     //identical to deleteTeam but with a path to a specific field inside the tName field
	if checkPlayer(pName) == true {
		_, err := FireDB.Collection(Col).Doc(pName).Update(Ctx, []firestore.Update{          //the specific field is determined by pokemonName
        	{
		        Path:  "teamList/tName/pokeList/pokemonName", //currently this function will always yield an error if the path is made specific like this, only teamList can be deleted as of now
                Value: firestore.Delete,                      //deletes the values inside a field

        	},})
       if err != nil {
            println("Error 500, unexpected error)", err)           //checks if data was fetched, prints this error if this fails
			return "something is wrong." 
		} else {println("Error 404, not found")                   //error code that is printed in discord bot if the player does not exist
	       return "Player was not found"}
}
return "Command finished."
}

func addItem(pName string, tName string, pokemonName string, iName string) string{

	// Get info from API

	var itemName string                                                            //variable that holds onto the item's name

	item := strings.Split(iName, " ")                                              //the item variab vill be given the name from iName

	// Depending on how many words the item is                                     //this code makes sure the item can have multiple names, for example rare candy, which has 2 names
	switch len(item) {
	case 1: itemName = item[0]
	case 2: itemName = item[0] + "-" + item[1]
	case 3: itemName = item[0] + "-" + item[1] + "-" + item[2]
	case 4: itemName = item[0] + "-" + item[1] + "-" + item[2] + "-" + item[3]
	}

	itemObject, ex := pokeapi.GetItem(itemName)                                          //checks teh item name in the api

	var effect string														// Makes one string out of the array with the effect info
	for i := 0; i < len(itemObject.ItemEffect); i++ {
		effect = itemObject.ItemEffect[i].ItemEffectInfo
	}

	if ex {                                                           //if the item was found inside the api, execute the following code
		_, err := FireDB.Collection("players").Doc(pName).Set(Ctx, map[string]interface{}{     //find the player document inside players collection in firebase database
			"playerName": pName,
			"teamList": []interface{}{
				map[string]interface{}{
				"teamName": tName,
				"pokeList": []interface{}{
					map[string]interface{}{
					"pokeName": pokemonName,
					"item": map[string]interface{}{                                       //the item field is initialised with the items name and other information such as category
						"item": iName,									                  //the cost of the item if its store bought, its set to 0 if the item is not buyable
						"category": itemObject.ItemCategory.CategoryName,                 //and its effect
						"cost": itemObject.ItemCost,
						"effect": effect,
							},
						},
					},
				},
			},
	    },firestore.MergeAll)
		if err != nil {
			println("Error 500, unexpected error)", err)           //checks if data was fetched, prints this error if this fails
			return "something is wrong." 
		}
		 return "item added"

	}else{println("Error 404, not found")                                       //if the item was not found in the pokemon api, this error is printed
		return "Item doesn't exist, or cannot be found in the API"}

}

func addMove(pName string, tName string, pokemonName string, mName string) string{  //adds a move to the pokemon  --currently pokemons can only hold one move at a time--

	moveObject, ex := pokeapi.GetMove(mName)                                            //the move is checked in the pokemon api to see if it exists

	var effect string														// Makes one string out of the array with the effect info
	for i := 0; i < len(moveObject.MoveEffectEntry); i++ {
		effect = moveObject.MoveEffectEntry[i].EffectInfo
	}

	if ex {                                                          //if the move exists, execute the code
		_, err := FireDB.Collection("players").Doc(pName).Set(Ctx, map[string]interface{}{  //find the player document inside players collection in firebase database
			"playerName": pName,
			"teamList": []interface{}{
				map[string]interface{}{
				"teamName": tName,
				"pokeList": []interface{}{
					map[string]interface{}{
					"pokeName": pokemonName,
					"moves": []interface{}{
						map[string]interface{}{                                                 //the moves field is updated with one move added to it
								"moveName": mName,
								"type": moveObject.MoveType.MoveTypeName,                       //the move information is automatically added from the pokemon api
								"pp": moveObject.MovePP,                                        //the moves pp
								"power": moveObject.MovePower,                                  //power
								"accuracy": moveObject.MoveAccuracy,                            //accuracy
								"effect": effect,                                    //and a description of what the move does
							},
							},
						},
					},
				},
			},
	    },firestore.MergeAll)
		if err != nil {
		   println("Error 500, unexpected error)", err)           //checks if data was fetched, prints this error if this fails
			return "something is wrong."
		}
	}else{println("Error 404, not found")                                       //if the move was not found in the pokemon api, this error is printed
		return "Move doesn't exist, or cannot be found in the API"}
		
		return "Move added"



}

// Gets the player data and returns it
func getAllPlayerData(pName string)   string {
	if checkPlayer(pName) == true {
		dsnap, err := FireDB.Collection("players").Doc(pName).Get(Ctx)
		m := dsnap.Data()
		y, _ := json.Marshal(m)
		var x string = "```" + string(y) + "```"

		return x

		if err != nil {
			println("Error 503, service is unavailable)", err) //checks if data was fetched, prints this error if this fails
			return "service is unavaliable."
		}

	}
	return "player does not exist"
}

func getPlayerData2(pName string) string {
	dsnap, err := FireDB.Collection("players").Doc(pName).Get(Ctx)
	if err != nil {}
	var p Players
	var x PkmnTeam
	var y Pokemon
	dsnap.DataTo(&p)
	dsnap.DataTo(&x)
	dsnap.DataTo(&y)

	var mong string

	mong = "```name: " + p.PlayerName + "\ncreator: " + p.Creator + "```"

	return mong



}

func GetPlayerTeamPokemon(pName string, tname string, pokeName string) string {
	dsnap, err := FireDB.Collection("players").Doc(pName).Get(Ctx)
	if err != nil {}
	var p Players
	var x PkmnTeam
	var y Pokemon
	dsnap.DataTo(&p)
	dsnap.DataTo(&x)
	dsnap.DataTo(&y)

	var poke string


	if checkPlayer(pName) {
		for i := 0; i > len(p.TeamList); i++ {
			if p.TeamList[i].TeamName == tname {
				for j := 0; j > len(p.TeamList[i].Pokemon); j++ {
					if p.TeamList[i].Pokemon[j].PokeName == pokeName {
						poke = "```name: " + p.TeamList[i].Pokemon[j].PokeName + "\ntype(s): " + p.TeamList[i].Pokemon[j].Type + "```"
						return poke
					}
					return "found no pokemon with that name on that team"
				}
			}
			return "found no team with that name for that player"
		}
	}
	return "player name no match"

}