package main

import (
	"pokebot"
	"context"
	//"fmt"
	//"log"
	//"net/http"
	//"os"
	"time"

	firebase "firebase.google.com/go"
	"google.golang.org/api/option"
)


func main() {
	pokebot.BotInit() // Starts the bot
	pokebot.Uptime = time.Now()
	pokebot.Ctx = context.Background()
	fbAccess := option.WithCredentialsFile(pokebot.DBpath) // Setting service account
	fbApp, err := firebase.NewApp(pokebot.Ctx, nil, fbAccess) // Creates firebase app
	if err != nil {
		println("An error has occurred: %s", err)   //if creating firebase app wont work, the service is unavailable and this error is returned
	}

	pokebot.FireDB, err = fbApp.Firestore(pokebot.Ctx) // Setting the client
	if err != nil {
		println("Error 503, service unavaliable)", err)           
	}


	defer pokebot.FireDB.Close() // Closing client
	pokebot.HandleRequest() // Starts the webAPI
	// Stops program from exiting, also does other things
	<-make(chan struct{})
	return
}
