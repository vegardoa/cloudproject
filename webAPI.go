package pokebot

import (
	"encoding/json"
	"google.golang.org/api/iterator"
	"log"
	"net/http"
	"strings"
)



// Gets data about specific user from firebase and sends it to browser
func handlerGetPlayerData(w http.ResponseWriter,r *http.Request, pName string){

	dsnap, err := FireDB.Collection("players").Doc(pName).Get(Ctx)
	if err != nil {
		http.Error(w, "The player was not found", http.StatusNotFound)
	}
	m := dsnap.Data()

	http.Header.Add(w.Header(), "content-type", "application/json")

	json.Marshal(m)
	json.NewEncoder(w).Encode(m)
}


// Sends all players and relevant data to browser
func handlerAllPlayers(w http.ResponseWriter,r *http.Request){

	dsnap := FireDB.Collection("players").Documents(Ctx)
	http.Header.Add(w.Header(), "content-type", "application/json")
	for {
		doc, err := dsnap.Next()
		if err == iterator.Done{
			break}
		if err != nil {
			http.Error(w, "The player list was not found", http.StatusNotFound)}

		m := doc.Data()
		json.Marshal(m)
		json.NewEncoder(w).Encode(m)

	}

}

func handlerPlayer(w http.ResponseWriter, r *http.Request){
	parts := strings.Split(r.URL.Path, "/")  // Checks the url for the name

	handlerGetPlayerData(w, r, parts[5]) // Sends the name and calls GetPlayerData

}

// HandleRequest handles api requests
func HandleRequest(){

	path := "/pokebot/v1/"
	http.HandleFunc(path + "players/specific/", handlerPlayer)
	http.HandleFunc(path + "players/all/", handlerAllPlayers)
	log.Fatal(http.ListenAndServe("127.0.0.1:8080", nil))

}
