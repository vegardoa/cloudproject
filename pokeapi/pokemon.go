package pokeapi

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

const pokeURL = "https://pokeapi.co/api/v2/pokemon/"

// Pokemon struct with info for each pokemon
type Pokemon struct {
	PokeName     string         `json:"name"`
	PokeID       int            `json:"id"`
	Species      PokemonSpecies `json:"species"`
	PokemonTypes []PokemonTypes `json:"Types"`
}

// AllMoves struct for printing all the moves of a pokemon
type AllMoves struct {
	PokemonMoves []PokemonMoves		`json:"moves"`
}

// PokemonSpecies struct to get the species name
type PokemonSpecies struct {
	SpeciesName  	string 				`json:"name"`
}

// PokemonTypes struct for the Types
type PokemonTypes struct {
	Type			TypeName			`json:"type"`
}

// PokemonMoves struct for the moves
type PokemonMoves struct {
	Move			MoveName 			`json:"move"`
}

// TypeName struct to get the name of the type
type TypeName struct {
	PokeTypeName 	string 				`json:"name"`
}

// MoveName struct to get the name of the move
type MoveName struct {
	PokeMoveName	string 				`json:"name"`
}

// GetPokemon retrieves pokemon info from PokeAPI
func GetPokemon(name string) (Pokemon, bool) {
	// Returns true if item exists and can be retrieved from the API
	boo := true
	response, err := http.Get(pokeURL + name)
	if err != nil {
		fmt.Println(err)
		boo = false
	}

	responseData, err := ioutil.ReadAll(response.Body)
	if err != nil {
		fmt.Println(err)
		boo = false
	}
	var responseObject Pokemon

	err = json.Unmarshal(responseData, &responseObject)
	if err != nil {
		fmt.Println(err)
		boo = false
	}

	return responseObject, boo
}

// GetPokemonMoves retrieves pokemon info from PokeAPI
func GetPokemonMoves(name string) (AllMoves, bool) {
	// Returns true if item exists and can be retrieved from the API
	boo := true
	response, err := http.Get(pokeURL + name)
	if err != nil {
		fmt.Println(err)
		boo = false
	}

	responseData, err := ioutil.ReadAll(response.Body)
	if err != nil {
		fmt.Println(err)
		boo = false
	}
	var responseObject AllMoves

	err = json.Unmarshal(responseData, &responseObject)
	if err != nil {
		fmt.Println(err)
		boo = false
	}

	return responseObject, boo
}