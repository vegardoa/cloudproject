package pokeapi

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

const itemURL = "https://pokeapi.co/api/v2/item/"

// ItemInfo struct for getting item info from pokeAPI
type ItemInfo struct {
	ItemCategory	ItemCategory	`json:"category"`
	ItemCost		int				`json:"cost"`
	ItemEffect		[]ItemEffect	`json:"effect_entries"`
}

// ItemCategory struct for getting the category of an item
type ItemCategory struct {
	CategoryName	string			`json:"name"`
}

// ItemEffect struct for getting the effect of an item
type ItemEffect struct {
	ItemEffectInfo		string			`json:"short_effect"`
}

// GetItem retrieves item info from PokeAPI
func GetItem(name string) (ItemInfo, bool) {
	// Returns true if item exists and can be retrieved from the API
	boo := true
	response, err := http.Get(itemURL + name)
	if err != nil {
		fmt.Println(err)
		boo = false
	}

	responseData, err := ioutil.ReadAll(response.Body)
	if err != nil {
		fmt.Println(err)
		boo = false
	}
	var responseObject ItemInfo
	err = json.Unmarshal(responseData, &responseObject)
	if err != nil {
		fmt.Println(err)
		boo = false
	}

	return responseObject, boo
}
