package pokeapi

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

const moveURL = "https://pokeapi.co/api/v2/move/"


// MoveInfo struct with info for each move
type MoveInfo struct {
	MoveAccuracy    int					`json:"accuracy"`
	MovePP          int 				`json:"pp"`
	MovePower       int 				`json:"power"`
	MoveType        MoveType			`json:"type"`
	MoveEffectEntry []MoveEffect		`json:"effect_entries"`
}

// MoveEffect struct for getting the Effect description
type MoveEffect struct {
	EffectInfo		string				`json:"short_effect"`
}

// MoveType struct for getting the type name of the move
type MoveType struct {
	MoveTypeName	string				`json:"name"`
}

// GetMove retrieves move info from PokeAPI
func GetMove(name string) (MoveInfo, bool) {
	// Returns true if move exists and can be retrieved from the API
	boo := true
	response, err := http.Get(moveURL + name)
	if err != nil {
		fmt.Println(err)
		boo = false
	}

	responseData, err := ioutil.ReadAll(response.Body)
	if err != nil {
		fmt.Println(err)
		boo = false
	}
	var responseObject MoveInfo

	err = json.Unmarshal(responseData, &responseObject)
	if err != nil {
		fmt.Println(err)
		boo = false
	}

	return responseObject, boo
}
